require 'highline/import'
require 'highline/simulate'
require_relative './test_helper'

class PairRankSimulatorTest < Minitest::Test
  include Rasam
  
  def setup
    input     = StringIO.new
    output    = StringIO.new
    $terminal = HighLine.new(input, output)
  end

  def test_simulator
    HighLine::Simulate.with('Bugs Bunny', '18') do
      name = ask('What is your name?')

      assert_equal 'Bugs Bunny', name

      age = ask('What is your age?')

      assert_equal '18', age
    end
  end

  def test_complete_pair_rank_session_when_there_is_no_tie
    HighLine::Simulate.with('A', 'A', 'B') do
      criteria = 'test'
      options = ['A', 'B', 'C']
      pr = PairRank.new(options)        
      
      pair = pr.combination
      first_choice = ask("What is your choice? : #{pair}")
      pr.make_rational_choice(pair, first_choice, criteria)

      assert_equal 1, pr.score_for('A')
      assert_equal 0, pr.score_for('B')
      assert_equal 0, pr.score_for('C')
      
      pair = pr.combination
      second_choice = ask("What is your choice? : #{pair}")
      pr.make_rational_choice(pair, second_choice, criteria)
      
      assert_equal 2, pr.score_for('A')
      assert_equal 0, pr.score_for('B')
      assert_equal 0, pr.score_for('C')
      
      pair = pr.combination
      third_choice = ask("What is your choice? : #{pair}")
      pr.make_rational_choice(pair, third_choice, criteria)
      
      assert_equal 2, pr.score_for('A')
      assert_equal 1, pr.score_for('B')
      assert_equal 0, pr.score_for('C')
      
      assert_equal [], pr.tied_pair
    end    
  end
  
  def test_complete_pair_rank_session_when_there_are_ties
    HighLine::Simulate.with('A', 'C', 'B', 'A', 'B', 'A') do
      
      criteria = 'test'
      options = ['A', 'B', 'C']
      pr = PairRank.new(options)        
      
      pair = pr.combination
      first_choice = ask("What is your choice? : #{pair}")
      pr.make_rational_choice(pair, first_choice, criteria)

      assert_equal 1, pr.score_for('A')
      assert_equal 0, pr.score_for('B')
      assert_equal 0, pr.score_for('C')
      
      pair = pr.combination
      second_choice = ask("What is your choice? : #{pair}")
      pr.make_rational_choice(pair, second_choice, criteria)
      
      assert_equal 1, pr.score_for('A')
      assert_equal 0, pr.score_for('B')
      assert_equal 1, pr.score_for('C')
      
      pair = pr.combination
      third_choice = ask("What is your choice? : #{pair}")
      pr.make_rational_choice(pair, third_choice, criteria)
      
      assert_equal 1, pr.score_for('A')
      assert_equal 1, pr.score_for('B')
      assert_equal 1, pr.score_for('C')
      
      assert_equal ['A', 'B'], pr.tied_pair
      
      pair = pr.combination
      fourth_choice = ask("What is your choice? : #{pair}")
      pr.make_rational_choice(pair, fourth_choice, criteria)

      assert_equal 2, pr.score_for('A')
      assert_equal 1, pr.score_for('B')
      assert_equal 1, pr.score_for('C')      
      
      assert_equal ['B', 'C'], pr.tied_pair
      
      pair = pr.combination
      fifth_choice = ask("What is your choice? : #{pair}")
      pr.make_rational_choice(pair, fifth_choice, criteria)

      assert_equal 2, pr.score_for('A')
      assert_equal 2, pr.score_for('B')
      assert_equal 1, pr.score_for('C')     
      
      assert_equal ['A', 'B'], pr.tied_pair
      
      pair = pr.combination
      final_choice = ask("What is your choice? : #{pair}")
      pr.make_rational_choice(pair, final_choice, criteria)      
      
      assert_equal 3, pr.score_for('A')
      assert_equal 2, pr.score_for('B')
      assert_equal 1, pr.score_for('C')         
      
      assert_equal [], pr.tied_pair
    end    
  end
end