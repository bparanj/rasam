# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rasam/version'

Gem::Specification.new do |spec|
  spec.name          = "rasam"
  spec.version       = Rasam::VERSION
  spec.authors       = ["Bala Paranj"]
  spec.email         = ["bparanj@gmail.com"]

  spec.summary       = %q{Pair Rank a given list of items.}
  spec.description   = %q{Select a single winner using votes that express preferences. This can also be used to create a sorted list of winners}
  spec.homepage      = "https://bitbucket.org/bparanj/rasam"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.required_ruby_version = '>= 2.2.2'

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 12.0"
  spec.add_development_dependency "minitest", "~> 5.10.3"
  spec.add_development_dependency "simplecov", "~> 0.11.2"
  spec.add_development_dependency "highline", "~> 1.7.8"
  spec.add_development_dependency "rubycritic", "~> 1.4"
end
