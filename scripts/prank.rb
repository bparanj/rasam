require 'highline/import'
require "pp"
require_relative '../lib/rasam'

include Rasam

def get_user_choice_for(pair)
  choose do |menu|
    menu.prompt = "Please choose your favorite: "
        
    pair.each do |c|
      menu.choice(c) do
        say(c)
        rationale = ask("Why?  ")
        say(rationale)
        
        @pr.make_rational_choice(pair, c, rationale)
      end
    end
  end
end

def display_score(options)
  options.each do |option|
    p "Score for #{option} : #{@pr.score_for(option)}"
  end     
end

options = ask("Enter your choices (or a blank line to quit):",
lambda { |ans| ans =~ /^-?\d+$/ ? Integer(ans) : ans} ) do |q|
  q.gather = ""
end

@pr = PairRank.new(options)

def display_decisions
  @pr.decisions.each do |d|
    p d.to_s
  end
end

pair = @pr.combination

loop do
  p pair    
  break if pair.nil?
  get_user_choice_for(pair) 
  pair = @pr.combination
end

display_decisions
display_score(options)

p 'Processing ties'

loop do
  tie = @pr.tied_pair
  if tie.empty?
    break
  else tie.empty?
    p 'Handling a tie'
  
    get_user_choice_for(tie)

    display_decisions    
    display_score(options)  
  end  
end

