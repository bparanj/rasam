# Rasam

Ranked pairs (RP) or the Tideman method is a voting system developed in 1987 by Nicolaus Tideman that selects a single winner using votes that express preferences. RP can also be used to create a sorted list of winners.

## Our Sponsor

This project is sponsored by Zepho Inc. If you are interested in learning Ruby / Rails, you can get a free 30 day trial to members only screencasts at : http://www.rubyplus.com

## Console

To experiment with the code, run `bin/console` for an interactive prompt.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rasam'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install rasam

## Usage

Here is a simple script that uses the highline gem for command line interface to the gem. Just enter Salary <enter>, Stocks <enter>, Telecommuting <enter> and hit <enter><enter> when prompted for choices:

```ruby
require 'highline/import'
require "pp"
require_relative '../lib/rasam'

include Rasam

def get_user_choice_for(pair)
  choose do |menu|
    menu.prompt = "Please choose your favorite: "
        
    pair.each do |c|
      menu.choice(c) do
        say(c)
        rationale = ask("Why?  ")
        say(rationale)
        
        @pr.make_rational_choice(pair, c, rationale)
      end
    end
  end
end

def display_score(options)
  options.each do |option|
    p "Score for #{option} : #{@pr.score_for(option)}"
  end     
end

options = ask("Enter your choices (or a blank line to quit):",
lambda { |ans| ans =~ /^-?\d+$/ ? Integer(ans) : ans} ) do |q|
  q.gather = ""
end

@pr = PairRank.new(options)

def display_decisions
  @pr.decisions.each do |d|
    p d.to_s
  end
end

pair = @pr.combination

loop do
  p pair    
  break if pair.nil?
  get_user_choice_for(pair) 
  pair = @pr.combination
end

display_decisions
display_score(options)

p 'Processing ties'

loop do
  tie = @pr.tied_pair
  if tie.empty?
    break
  else tie.empty?
    p 'Handling a tie'
  
    get_user_choice_for(tie)

    display_decisions    
    display_score(options)  
  end  
end
```

You can find this script prank.rb in the scripts folder.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

Run the test with color:

```ruby
$ ruby -rminitest/pride test/pair_rank_test.rb --verbose
$ ruby -rminitest/pride test/pair_rank_simulator_test.rb --verbose
$ ruby -rminitest/pride test/combination_test.rb --verbose
```

To run all tests:

```ruby
$ rake
```

Default rake task will not run all the tests if you mix Test::Unit::TestCase and Minitest::Test. It will only run tests with Test::Unit::TestCase as it's parent. The coverage report gets clobbered if all the tests extend from Test::Unit::TestCase. So one of the test is extending from Minitest::Test to generate the coverage report.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Articles

[TDD Beyond Basics : Outside In Perspective] (https://rubyplus.com/articles/2531 'TDD Beyond Basics : Outside In Perspective')

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/bparanj/rasam.

gem build rasam.gemspec 
gem push rasam-0.1.0.gem

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).